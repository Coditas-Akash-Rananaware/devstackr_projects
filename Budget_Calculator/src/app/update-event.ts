import { BudgetItem } from '../shared/models/budget-item.model';

export interface UpdateEvent {
  old: BudgetItem;
  new: BudgetItem;
}
